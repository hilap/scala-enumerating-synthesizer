import appassembler._
import sbt._
import Keys._
//import sbtassembly.Plugin._
//import AssemblyKeys._
import SbtAppAssemblerPlugin._

// https://mvnrepository.com/artifact/com.github.scopt/scopt_2.11
scalaVersion := "2.11.7"
// https://mvnrepository.com/artifact/org.scala-lang/scala-compiler
libraryDependencies += "org.scala-lang" % "scala-compiler" % "2.11.7"

libraryDependencies += "com.github.scopt" % "scopt_2.11" % "3.3.0"

// https://mvnrepository.com/artifact/org.scala-lang/scala-swing
libraryDependencies += "org.scala-lang" % "scala-swing" % "2.11.0-M7"


name := "gim-enum"

appAssemblerSettings

/*appAssemblerJvmOptions := Seq(
  "-Xms1024M",
  "-Xmx1024M",
  "-Xss1M",
  "-XX:MaxPermSize=256M",
  "-XX:+UseParallelGC"
)*/

appPrograms := Seq(Program("phd.GIM.Main"))
/*
lazy val buildSettings = Seq(
  version := "0.1-SNAPSHOT",
  name := "gim-enum",
  scalaVersion := "2.11.7"
)

val app = (project in file("app")).
  settings(buildSettings: _*).
  settings(assemblySettings: _*).
  settings(mainClass in assembly := Some("phd.GIM.Main"))
.settings(
mergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x =>
    val oldStrategy = (mergeStrategy in assembly).value
    oldStrategy(x)
}
).settings(jarName in assembly := "gim-enum.jar")*/
