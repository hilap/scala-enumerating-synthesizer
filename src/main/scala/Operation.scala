package phd.GIM
/**
 * Created by hila on 05/10/2016.
 */
trait Operation{
  def isNotFiltered(program : Seq[Transform]) : Boolean
  def isValidProgram(program : Seq[Transform]) : Boolean
}

final case class AddExample(input : String, output : String) extends Operation {
  override def isNotFiltered(program: Seq[Transform]): Boolean = true

  override def isValidProgram(program: Seq[Transform]): Boolean = true
}
final case class Affix(prefix : Seq[Transform]) extends Operation {
  override def isNotFiltered(program: Seq[Transform]): Boolean = isValidProgram(program)

  override def isValidProgram(program: Seq[Transform]): Boolean = program.length >= prefix.length && prefix.zip(program).forall(pair => pair._1 == pair._2)
}
final case class Retain(operationSequence : Seq[Transform]) extends Operation {
  override def isNotFiltered(program: Seq[Transform]): Boolean = true //doesn't mean some further enumerated program won't have the sequence, can't rule out prefix

  override def isValidProgram(program: Seq[Transform]): Boolean = Operation.hasSequence(program,operationSequence)
}
final case class Exclude (operationSequence : Seq[Transform]) extends Operation {
  override def isNotFiltered(program: Seq[Transform]): Boolean = isValidProgram(program)

  override def isValidProgram(program: Seq[Transform]): Boolean = !(Operation.hasSequence(program,operationSequence))
}

final case object Finished extends Operation {
  override def isNotFiltered(program: Seq[Transform]): Boolean = false
  override def isValidProgram(program: Seq[Transform]): Boolean = false
}

object Operation{
  def hasSequence(program: Seq[Transform], operationSequence: Seq[Transform]): Boolean = {
    var i = program.indexOf(operationSequence(0))
    while(i != -1) {
      val paired = program.drop(i).zip(operationSequence)
      if (paired.length == operationSequence.length && paired.forall(pair => pair._1 == pair._2))
        return true
      i = program.indexOf(operationSequence(0),i+1)
    }
    return false
  }

}