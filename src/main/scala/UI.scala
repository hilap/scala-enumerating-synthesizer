package phd.GIM
/**
 * Created by hila on 20/09/2016.
 */

import javax.swing.event.ListSelectionEvent
import javax.swing.table.{DefaultTableModel, TableModel}

import scala.collection.mutable.ListBuffer
import scala.swing._
import scala.swing.event.ListSelectionChanged

class UI(program : List[Transform], inputs: List[AnyRef],values : List[List[AnyRef]]) extends Frame {

  //override def top: Frame = new MainFrame {
    title = "GIM4GT"
    //override  def closeOperation() : Unit = {}


    val tab = new Table(program.length + 1,inputs.length + 1) {
      model = new DefaultTableModel(program.length + 1,inputs.length + 1) {
        override def isCellEditable(row : Int, col : Int) = false
      }
      autoResizeMode = Table.AutoResizeMode.Off

      def enableAllSyntaxOperations() = {
        val p = menuBar.menus.find(m => m.text == "Syntax Operations").get.peer
        for (i <- 0 until p.getItemCount) p.getItem(i).setEnabled(true)
      }

      def disableAffix() = {
        val p = menuBar.menus.find(m => m.text == "Syntax Operations").get.peer
        val aff = (0 until p.getItemCount).filter(i => p.getItem(i).getText == "Affix").head
        p.getItem(aff).setEnabled(false)
      }

      def enableAffix() = {
        val p = menuBar.menus.find(m => m.text == "Syntax Operations").get.peer
        val aff = (0 until p.getItemCount).filter(i => p.getItem(i).getText == "Affix").head
        p.getItem(aff).setEnabled(true)
      }

      def disableAllSyntaxOperations(): Unit = {
        val p = menuBar.menus.find(m => m.text == "Syntax Operations").get.peer
        for (i <- 0 until p.getItemCount) p.getItem(i).setEnabled(false)
      }


      def isSequenceSelected : Boolean = selection.rows.toList.sorted.zip(selection.rows.toList.sorted.drop(1)).map(p => p._2 - p._1 == 1).forall(b => b)
      selection.reactions += {
        case event.TableRowsSelected(_,_,_) => {
          //if selection size is > 1
          if (selection.rows.size > 1 && 
            !isSequenceSelected) {
            disableAllSyntaxOperations()
          }
          else if (selection.rows.size == 1) {
            enableAllSyntaxOperations()
            if (!selection.rows.contains(0)) {
              disableAffix()
            }
          }
          else {
            if (isSequenceSelected && (selection.rows.min == 0 || selection.rows.min == 1)) {
              enableAffix()
            } else disableAffix()
          }
        }
      }
    }

  def refreshOpList(): Unit = {
    val view = grid.contents(1).asInstanceOf[ListView[String]]
    view.listData = operations.map(op => op.toString).toList
  }


  val grid = new GridPanel(1,2)
  grid.contents += new ScrollPane(tab) {
    horizontalScrollBarPolicy = ScrollPane.BarPolicy.Always
  }
  grid.contents += new ListView[String]()


    contents = new BorderPanel {
      //layout(tab) = scala.swing.BorderPanel.Position.Center
      layout(grid) = scala.swing.BorderPanel.Position.Center
    }
    size = new Dimension(300, 200)
    def selectedTransforms = tab.selection.rows.toList.sorted.map(i => i-1).filter(i => i >= 0).map(i => program(i))
    menuBar = new MenuBar {
      contents += new Menu("File") {
        contents += new MenuItem(Action("Export code"){
          operations.clear()
          operations += Finished
          refreshOpList()
        })
      }
      contents += new Menu("Syntax Operations") {
        contents += new MenuItem(Action("Affix"){
          operations += Affix(selectedTransforms)
          refreshOpList()
        })
        contents += new MenuItem(Action("Retain"){
          operations += Retain(selectedTransforms)
          refreshOpList()
        })
        contents += new MenuItem(Action("Exclude"){
          operations += Exclude(selectedTransforms)
          refreshOpList()
        })
        contents += new MenuItem("Add (force)")
      }

      contents += new Menu("Data operations") {

        contents += new MenuItem(Action("New Example"){
          val inputRet = Dialog.showInput(this,"Input","",Dialog.Message.Question,Swing.EmptyIcon,Seq(),"")
          inputRet match {
            case Some(input) => {
              val outputRet = Dialog.showInput(this,"Output","",Dialog.Message.Question,Swing.EmptyIcon,Seq(),"")
              outputRet match {
                case Some(output) => operations += AddExample(input,output)
                case None =>
              }
            }
            case None =>
          }
          refreshOpList()
        })
        contents += new MenuItem("Force Intermediate State")
      }
      contents += new Menu("Learn"){
        contents += new MenuItem("Learn")
      }
    }
  //}

  tab.update(0,0,"input")
  (1 to inputs.length).foreach {i => tab.update(0,i,inputs(i-1))}
  (1 to program.length).foreach{ i=>
    tab.update(i,0,program(i-1).op)
    (1 to inputs.length).foreach { j =>
      tab.update(i, j, values(i - 1)(j-1))
    }
  }

  private val operations = ListBuffer[Operation]()
  def requestedOperations() : Iterable[Operation] = operations
}
