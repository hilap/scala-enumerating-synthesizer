package phd.GIM
/**
 * Created by hila on 22/03/2016.
 */

import java.awt.event.WindowEvent
import java.io._
import java.util.NoSuchElementException
import java.util.concurrent.CountDownLatch
import javax.script.{ScriptContext, Bindings, Compilable, ScriptEngineManager}

import sun.awt.WindowClosingListener

import scala.annotation.tailrec
import scala.collection
import scala.collection.GenIterable
import scala.collection.mutable.ListBuffer
import scala.collection.parallel.mutable
import scala.reflect.ClassTag
import scala.tools.nsc.Global
import scala.tools.nsc.interpreter.IMain
import scala.util.control.Breaks
import scala.util.{Failure, Success, Try}


object Main extends App {
  val Output = new PrintStream(new FileOutputStream("out.txt",true))
  val config : Config = CmdLine.parser.parse(args, Config()) match {
    case Some(cfg) => cfg
    case None => {
      System.exit(1)
      Config() //dead code
    }
  }
  val engine = new ScriptEngineManager().getEngineByName("scala")
  val settings = engine.asInstanceOf[scala.tools.nsc.interpreter.IMain].settings
  val scalaJar = System.getProperty("java.class.path").split(java.io.File.pathSeparator)
    .filter {
      _.contains("scala-library")
    }.head
  settings.bootclasspath.append(scalaJar)
  settings.classpath.append(scalaJar)
  Output.println(settings)

  def setup(in : List[String]) : List[AnyRef] = {
    engine.eval("case class Tweet(status : String)\n" +
      "object PimpMean{\n  def average[T]( ts: Iterable[T] )( implicit num: Numeric[T] ) = {\n    num.toDouble( ts.sum ) / ts.size\n  }\n  implicit def iterebleWithAvg[T:Numeric](data:Iterable[T]) = new {\n    def mean = average(data)\n  }\n}\nimport PimpMean.iterebleWithAvg") //TODO: output to "definitions" file
    engine.eval(
      in.mkString("val inputs = List(",",",")")
    ).asInstanceOf[List[AnyRef]]
  }

  var (inList,outList) = {
    val ioPairs = scala.io.Source.fromFile(config.examplesFile).getLines().map(l => l.trim().split(("\t"))).toList
    if (ioPairs.exists(p => p.length != 2))
      throw new Exception("malformed example file")

    (ioPairs.map(p => p(0)).toList, ioPairs.map(p => p(1)).toList)
  }

  var outputs = engine.eval(
    outList.mkString("List(",",",")")
  ).asInstanceOf[List[AnyRef]]
  var inputs = setup(inList)


  Output.println(inputs)
  Output.println(" |")
  Output.println(" V")
  Output.println(outputs)

  val transformations = scala.io.Source.fromFile(config.vocabularyFile).getLines.map(l => Transform(l)).toList

  /*List(

  )*/
 /* List(Transform("map(_.toUpper)"),
  Transform("map(_.take(1)"),
  Transform("map(_.drop(1)"),

  )
*/

  final case class ExecutedProgram(ops: List[Transform], results: List[List[AnyRef]])
  @SerialVersionUID(100L)
  final case class StoredProgram(ops: List[Transform], results : List[List[String]])
  object ExecutedProgram{
    def reEvaluateProgramSteps(oldProgram: ExecutedProgram) : ExecutedProgram = ExecutedProgram(oldProgram.ops,
      (1 to oldProgram.ops.length).map(substringLength => tryEvaluating(oldProgram.ops.take(substringLength)).get).toList //they've all previously compiled, .get should be fine
    )

    def extend(program : ExecutedProgram, extendByOp : Transform, extendByResult : List[AnyRef]): ExecutedProgram = {
      ExecutedProgram(program.ops :+ extendByOp, program.results :+ extendByResult)
    }

    def toStoredProgram(program : ExecutedProgram) = StoredProgram(program.ops,program.results.map(s => s.map(o => o.toString)))
  }

  object Programs extends Iterator[ExecutedProgram]{
    override def toString = "iterator"
    final case class UnexecutedProgram(previousProgram: ExecutedProgram, extendedBy : Transform) {
      def ops = previousProgram.ops :+ extendedBy
    }
    private var curr : Option[ExecutedProgram] = None
    private var currLevel = collection.mutable.Queue[UnexecutedProgram](transformations.map(transformation => UnexecutedProgram(ExecutedProgram(List(),List()),transformation)):_*)
    private var nextLevel = collection.mutable.Queue[UnexecutedProgram]()

    private val filters = ListBuffer[Operation]()
    def addFilter(op: Operation): Unit = {
      filters += op
      op match {
        case forwardOnly @ (_:Affix|_:Retain|_:Exclude) => {
          currLevel = currLevel.filter(prog => op.isNotFiltered(prog.ops))
          nextLevel = nextLevel.filter(prog => op.isNotFiltered(prog.ops))
          if (currLevel.isEmpty) {
            moveToNextLevel()
          }
        }
        case _ =>
      }
    }

    override def next(): ExecutedProgram = curr match {
      case None => if (hasNext) curr.get else throw new NoSuchElementException()
      case Some(x) => {
        curr = None
        x
      }
    }

    override def hasNext: Boolean = curr match {
      case None => if (currLevel.isEmpty)
          false
        else {
          do {
            val head = currLevel.dequeue()
            tryEvaluating(head.ops) match {
              case Success(newProgramResult) => {
                val newProgram = ExecutedProgram.extend(head.previousProgram, head.extendedBy, newProgramResult)
                val programs = extendByOne(newProgram)
                nextLevel.enqueue(programs: _*)
                if (filters.forall(f => f.isValidProgram(newProgram.ops))) {
                  curr = Some(newProgram)
                }
              }
              case _ =>
            }
            if (currLevel.isEmpty) {
              moveToNextLevel()
            }
          } while (!(currLevel.isEmpty && nextLevel.isEmpty) && curr.isEmpty)
          !curr.isEmpty
        }
      case Some(x) => true
    }

    private def moveToNextLevel() = {
      currLevel = nextLevel
      nextLevel = collection.mutable.Queue[UnexecutedProgram]()
      engine.asInstanceOf[IMain].reset()
      setup(inList)
    }

    private def extendByOne(curr: ExecutedProgram) = transformations
      .filter(transformation => curr.ops.isEmpty || transformation != curr.ops.last)
      .map(newOp => UnexecutedProgram(curr,newOp))
      .filter(newProgram => filters.forall(f => f.isNotFiltered(newProgram.ops)))

      /*.map { t =>
        val op = "inputs.map(input => input." + (curr.ops :+ t).map(_.op).mkString(".") + ")"
        println(op)
        val res = Try(engine.eval(op)
          //eval("prev.asInstanceOf[" + typeToString(prev) + "]." + t.op, bindings)
        )
        (t, res)
      }.filter(t => t._2.isSuccess && (curr.ops.isEmpty || t._2.get != curr.results.last))
        .map(t =>
          ExecutedProgram(curr.ops :+ t._1, curr.results :+ t._2.get.asInstanceOf[List[AnyRef]]))
    }*/
  }

  def typeToString(prev : AnyRef) = {
    def simpleName(sn : String) = if (sn.contains("$"))
      sn.substring(sn.lastIndexOf('$') + 1)
    else sn
    def innerType(prev : Iterable[_]) = if (prev.isEmpty)
      "_"
    else {
      val sn = prev.head.getClass.getName
      simpleName(sn)
    }
    def innerTypeMap(prev : Map[_,_]) = if (prev.isEmpty)
      "_,_"
    else {
      val n1 = simpleName(prev.head._1.getClass.getName)
      val n2 = simpleName(prev.head._1.getClass.getName)
      n1 + "," + n2
    }
    prev match {
      case _ : List[_] => "List[" + innerType(prev.asInstanceOf[Iterable[_]]) + "]"
      case _ : Map[_,_] => "Map[" + innerTypeMap(prev.asInstanceOf[Map[_,_]]) + "]"
      case _ : Iterable[_] => "Iterable[" + innerType(prev.asInstanceOf[Iterable[_]]) + "]"
      case ct : scala.reflect.ClassTag[_] => ct.toString()
      case _ => "_"
    }
  }

  def tryEvaluating(transformations: List[Transform]): Try[List[AnyRef]] = {
    val op = "inputs.map(input => input." + transformations.map(_.op).mkString(".") + ")"
    //println(op)
    Try(engine.eval(op)).map(_.asInstanceOf[List[AnyRef]])
  }


/*  val ui = new UI(toy.ops, inputs, toy.results)
  val flag = new CountDownLatch(1);
  ui.reactions += {
    case e : scala.swing.event.WindowClosing => {
      flag.countDown()
    }
    case _ =>
  }
  ui.visible = true
  flag.await()
  ui.requestedOperations().foreach { op => op match {
    case AddExample(in, out) => {
      inList = inList :+ in
      inputs = setup(inList).asInstanceOf[List[AnyRef]]
      outputs = outputs :+ engine.eval(out)
    }
    case _ => Programs.addFilter(op)
  }
  }
  println(inputs)
  println(typeToString(inputs))
  println(outputs)
  println(typeToString(outputs))

  val ep = tryEvaluating(List(Transform("map(_.status)",false),
    Transform("""flatMap(_.split("[^\\w#]+"))""",true),
    Transform("""filter(_.startsWith("#"))""",true),
    Transform("map(_.toLowerCase)",true),
    Transform("map(_.drop(1))",false),
    Transform("groupBy(x => x)", false),
    Transform("map(kv => kv._1 -> kv._2.length)", false)))

  println(ep)
  println(ep.get == outputs)*/


  @SerialVersionUID(103L)
  final case class Step(program : StoredProgram, userOps : List[Operation])


  var userSteps = if (config.programStepUserFile.isEmpty && config.enumTil.isEmpty)
      List()
  else{
    val fname = if (!config.programStepUserFile.isEmpty) config.programStepUserFile
                else config.enumTil
    val ois = new ObjectInputStream(new FileInputStream(fname))
    val steps = ois.readObject.asInstanceOf[List[Step]]
    ois.close
    steps
  }

  val programSteps = ListBuffer[Step]()
  var programsTested : Long = 0
  while(Programs.hasNext){
    val nextProgram : ExecutedProgram = Programs.next
    val prog = if (nextProgram.results.exists(resultStep => resultStep.length != inputs.length))
        ExecutedProgram.reEvaluateProgramSteps(nextProgram)
      else nextProgram
    programsTested = programsTested + 1
    if (!nextProgram.results.isEmpty && nextProgram.results.last == outputs) {
      //found program!
      //UI or user steps from file?
      val currentStepOperations = if (userSteps.isEmpty) {
        //UI

        val ui = new UI(prog.ops, inputs, prog.results)
        val flag = new CountDownLatch(1);
        ui.reactions += {
          case e: scala.swing.event.WindowClosing => {
            flag.countDown()
          }
          case _ =>
        }
        ui.visible = true
        flag.await()
        ui.requestedOperations()
      }
      //else from file
      else {
        if (!config.enumTil.isEmpty) {
          if (userSteps.last.program.ops == nextProgram.ops) {//can stop
            List(Finished)
          }
          else List()
        }
        else {
          assert(nextProgram.ops == userSteps.head.program.ops)
          val ret = userSteps.head.userOps
          userSteps = userSteps.tail
          ret
        }
      }

      currentStepOperations.foreach { op => op match {
        case AddExample(in, out) => {
          inList = inList :+ in
          inputs = setup(inList)
          outputs = outputs :+ engine.eval(out)
          Output.println(inputs)
          Output.println(typeToString(inputs))
          Output.println(outputs)
          Output.println(typeToString(outputs))
        }
        case Finished => {

          //output
          programSteps += Step(ExecutedProgram.toStoredProgram(prog), List(Finished))
          if (!config.programStepOutputFile.isEmpty) {
            val oos = new ObjectOutputStream(new FileOutputStream(config.programStepOutputFile))
            oos.writeObject(programSteps.toList)
            oos.close
          }
          //print data
          Output.print("Programs tested: ")
          Output.println(programsTested)
          Output.print("Num steps w/ user: ")
          Output.println(programSteps.length)
          Output.println(prog)
          sys.exit(0)
        }
        case _ => Programs.addFilter(op)
      }
      }
      programSteps += Step(ExecutedProgram.toStoredProgram(prog), currentStepOperations.toList)
      Output.println(programSteps)
//      if (currentStepOperations.isEmpty) {
//        Breaks.break()
//      }
    }

  }
  Output.println("stopped")
  Output.println(programSteps)


  sys.exit(0)
}

@SerialVersionUID(101L)
case class Transform(op : String, isIdempotent : Boolean = false) extends Serializable{
  override def equals(o: Any) = o match {
    case Transform(_op,_isId) => _op == op
    case _ => false
  }
}