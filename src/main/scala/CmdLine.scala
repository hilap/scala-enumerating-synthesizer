package phd.GIM

/**
 * Created by hila on 08/07/2015.
 */
import scopt._

case class Config(vocabularyFile : String = "", examplesFile : String = "", programStepUserFile : String = "", programStepOutputFile : String = "", enumTil : String = "")
object CmdLine {
  val parser = new OptionParser[Config]("gim-enumerator") {
    head("gim-enumerator","1.0")
    help("help") text("Get enumerator usage")
    opt[String]('v', "vocab") required() valueName ("<vocab-file>") action ((x, c) => c.copy(vocabularyFile = x)) text (
      "path to file containing the vocabulary to enumerate")
    opt[String]('e', "examples") required() valueName("<examples-file>") action ((x,c) => c.copy(examplesFile =  x)) text ("file with input output pairs, one pair per line, tab delimited")
    opt[String]('l', "live") optional() valueName("<steps-output-file>") action ((x, c) =>
      c.copy(programStepOutputFile = x)) text ("run with user, save output to file")
    opt[String]('s', "steps") optional() valueName ("<steps-input-file>") action ((x, c) => c.copy(programStepUserFile = x)) text (
      "run from user file"
      )
    opt[String]('u', "enum-until") optional() valueName ("<steps-input-file>") action ((x,c) => c.copy(enumTil = x)) text ("enumerate programs until result from steps file")


    checkConfig( c => if (!(new java.io.File(c.vocabularyFile).exists()))
      failure("vocabulary file does not exist")
    else if (c.programStepOutputFile.isEmpty && c.programStepUserFile.isEmpty)
      failure ("no operation mode specified")
    else if (!c.programStepOutputFile.isEmpty && !c.programStepUserFile.isEmpty)
      failure("only one operation can be specified")
    else if (!c.enumTil.isEmpty && c.programStepOutputFile.isEmpty) {
      failure("enum-until mode requires an output file, specify with -l")
    }
    else if (!c.programStepOutputFile.isEmpty && (new java.io.File(c.programStepOutputFile).exists()))
      failure("output file already exists")
    else if (!c.programStepUserFile.isEmpty && !(new java.io.File(c.programStepUserFile).exists()))
      failure("user file doesn't exist")
    else if (!c.enumTil.isEmpty && !(new java.io.File(c.enumTil).exists()))
      failure("user steps file to enum until doesn't exist")
    else success)
  }
}
